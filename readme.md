# Perch Sublime Completions

This is a small work-in-progress of completions for Sublime Text 3. The file contains some useful snippets for use when building in Perch CMS. 

## Installation

For Windows users, place the file at: `C:\Users\username\AppData\Roaming\Sublime Text 3\Packages\User`

OSX users, place the file at: TBC

## Triggers

*ALL of the follow must be prefixed manually with an opening `<?php`.*

### perch_content()

`pcontent` - Adds a content region.

### perch_content_custom()

`pcontentcustom` - Used to add a content custom region function.

`pcontentcreate`

Used to programmatically create a content region.

`playout`

Calls a layout in to your page.

`playoutarr`

Used to pass any value into the layout using an array.

`ppageattr`
`ppageattrs`
`pnavigation`

### Additions

As I stated above, this is very much a work-in-progress. It will be updated regularly as I find more useful snippets to add.